﻿using System;

namespace SummaChiselClass
{
    class Program
    {
        static void Main(string[] args)
        {
            Numbers mas1 = new Numbers(InputSizeArray("Введите размерность первого числа: "));
            FillingArray(mas1, "Введите первое число последовательно по одному знаку: ");
            Numbers mas2 = new Numbers(InputSizeArray("Введите размерность второго числа: "));
            FillingArray(mas2, "Введите второе число последовательно по одному знаку: ");

            Numbers mas3 = mas1 + mas2;

            mas3.PrintArray();

            Console.ReadKey();
        }

        static int InputSizeArray(string message)
        {
            int number;
            bool check;

            do
            {
                Console.Write(message);
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false);

            return number;
        }

        static void FillingArray(Numbers mas, string message)
        {
            Console.WriteLine(message);

            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = InputNumberesArray(i);
            }
        }

        static int InputNumberesArray(int index)
        {
            int number;
            bool check;

            do
            {
                Console.Write("[" + (index + 1) + "]: ");
                check = int.TryParse(Console.ReadLine(), out number);

            } while (check == false || number < -1 || number > 10);

            return number;
        }

    }
}
