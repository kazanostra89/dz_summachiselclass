﻿using System;

namespace SummaChiselClass
{
    class Numbers
    {
        private int[] number;
        private int lengthNumber;


        public Numbers(int size)
        {
            number = new int[size];
            lengthNumber = size;
        }


        public void PrintArray()
        {
            Console.WriteLine();

            for (int i = 0; i < number.Length; i++)
            {
                Console.Write(number[i]);
            }
        }

        private static void ResizeArray(ref Numbers number, int ostatok)
        {
            Numbers newNumber = new Numbers(number.Length + 1);
            int indexOld = 0;

            for (int i = 0; i < newNumber.Length; i++)
            {
                if (i == 0)
                {
                    newNumber[i] = ostatok;
                }
                else
                {
                    newNumber[i] = number[indexOld];
                    indexOld++;
                }
            }

            number = newNumber;
        }


        public int Length
        {  
            get { return lengthNumber; }
        }

        public int this[int index]
        {
            get { return number[index]; }

            set { number[index] = value; }
        }


        public static Numbers operator +(Numbers num1, Numbers num2)
        {
            int sizeNewNumber, selector, ostatok, summProm;
            ostatok = 0;

            #region Определение размера формируемого массива
            if (num1.Length == num2.Length)
            {
                sizeNewNumber = num1.Length;
                selector = 1;
            }
            else if (num1.Length > num2.Length)
            {
                sizeNewNumber = num1.Length;
                selector = 2;
            }
            else
            {
                sizeNewNumber = num2.Length;
                selector = 3;
            }
            #endregion

            Numbers summaMas = new Numbers(sizeNewNumber);

            #region Выбор варианта рассчета суммы в зависимости от размерности чисел
            switch (selector)
            {
                case 1:
                    for (int i = (sizeNewNumber - 1); i >= 0; i--)
                    {
                        summProm = num1[i] + num2[i] + ostatok;

                        if (summProm >= 10)
                        {
                            summaMas[i] = summProm % 10;
                            ostatok = summProm / 10;
                            if (i == 0) ResizeArray(ref summaMas, ostatok);
                        }
                        else
                        {
                            summaMas[i] = summProm;
                            ostatok = 0;
                        }
                    }
                    break;
                case 2:
                    for (int lengthMax = sizeNewNumber - 1, lengthMin = num2.Length - 1; lengthMax >= 0; lengthMax--, lengthMin--)
                    {
                        if (lengthMin >= 0)
                        {
                            summProm = num1[lengthMax] + num2[lengthMin] + ostatok;

                            if (summProm >= 10)
                            {
                                summaMas[lengthMax] = summProm % 10;
                                ostatok = summProm / 10;
                            }
                            else
                            {
                                summaMas[lengthMax] = summProm;
                                ostatok = 0;
                            }
                        }
                        else if (lengthMax >= 0)
                        {
                            summProm = num1[lengthMax] + ostatok;

                            if (summProm >= 10)
                            {
                                summaMas[lengthMax] = summProm % 10;
                                ostatok = summProm / 10;
                                if (lengthMax == 0) ResizeArray(ref summaMas, ostatok);
                            }
                            else
                            {
                                summaMas[lengthMax] = summProm;
                                ostatok = 0;
                            }
                        }
                    }
                    break;
                case 3:
                    for (int lengthMax = sizeNewNumber - 1, lengthMin = num1.Length - 1; lengthMax >= 0; lengthMax--, lengthMin--)
                    {
                        if (lengthMin >= 0)
                        {
                            summProm = num1[lengthMin] + num2[lengthMax] + ostatok;

                            if (summProm >= 10)
                            {
                                summaMas[lengthMax] = summProm % 10;
                                ostatok = summProm / 10;
                            }
                            else
                            {
                                summaMas[lengthMax] = summProm;
                                ostatok = 0;
                            }
                        }
                        else if (lengthMax >= 0)
                        {
                            summProm = num2[lengthMax] + ostatok;

                            if (summProm >= 10)
                            {
                                summaMas[lengthMax] = summProm % 10;
                                ostatok = summProm / 10;
                                if (lengthMax == 0) ResizeArray(ref summaMas, ostatok);
                            }
                            else
                            {
                                summaMas[lengthMax] = summProm;
                                ostatok = 0;
                            }
                        }
                    }
                    break;
            }
            #endregion

            return summaMas;
        }

        public static bool operator ==(Numbers num1, Numbers num2)
        {
            bool result = false;

            if (num1.number.Length == num2.number.Length)
            {
                for (int i = 0; i < num1.number.Length; i++)
                {
                    if (num1.number[i] == num2.number[i])
                    {
                        result = true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return result;
        }

        public static bool operator !=(Numbers num1, Numbers num2)
        {
            bool result = false;

            if (num1.number.Length != num2.number.Length)
            {
                return true;
            }
            else if (num1.number.Length == num2.number.Length)
            {
                for (int i = 0; i < num1.number.Length; i++)
                {
                    if (num1.number[i] == num2.number[i])
                    {
                        result = false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }

            return result;
        }

        public static bool operator >(Numbers num1, Numbers num2)
        {
            if (num1.number.Length > num2.number.Length)
            {
                return true;
            }
            else if (num1.number.Length == num2.number.Length)
            {
                for (int i = 0; i < num1.number.Length; i++)
                {
                    if (num1.number[i] > num2.number[i])
                    {
                        return true;
                    }
                    else if (num1.number[i] < num2.number[i])
                    {
                        return false;
                    }
                }
            }

            return false;
        }

        public static bool operator <(Numbers num1, Numbers num2)
        {
            if (num1.number.Length < num2.number.Length)
            {
                return true;
            }
            else if (num1.number.Length == num2.number.Length)
            {
                for (int i = 0; i < num1.number.Length; i++)
                {
                    if (num1.number[i] < num2.number[i])
                    {
                        return true;
                    }
                    else if (num1.number[i] > num2.number[i])
                    {
                        return false;
                    }
                }
            }

            return false;
        }

        public static bool operator >=(Numbers num1, Numbers num2)
        {
            if (num1.number.Length > num2.number.Length)
            {
                return true;
            }
            else if (num1.number.Length < num2.number.Length)
            {
                return false;
            }
            else
            {
                for (int i = 0; i < num1.number.Length; i++)
                {
                    if (num1.number[i] > num2.number[i])
                    {
                        return true;
                    }
                    else if (num1.number[i] < num2.number[i])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public static bool operator <=(Numbers num1, Numbers num2)
        {
            if (num1.number.Length < num2.number.Length)
            {
                return true;
            }
            else if (num1.number.Length > num2.number.Length)
            {
                return false;
            }
            else
            {
                for (int i = 0; i < num1.number.Length; i++)
                {
                    if (num1.number[i] < num2.number[i])
                    {
                        return true;
                    }
                    else if (num1.number[i] > num2.number[i])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

    }
}
